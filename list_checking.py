'''
Take a list, say for example this one:

  a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
  
and write a program that prints out all the elements of the list that are less than 5.

Extras:

Instead of printing the elements one by one, make a new list that has all the elements less than 5 from this list in it and print out this new list.
Write this in one line of Python.
Ask the user for a number and return a list that contains only elements from the original list a that are smaller than that number given by the user.
'''
# # SOLUTION 1
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# number = int(input('Enter a filter number: '))
# array_length = len(a)
# temp_array = []
# for i in range(array_length - 1):    
#     if a[i] < number:
#         temp_array.append(a[i])        
# print(temp_array)

#SOLUTION 2
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
number = int(input('Enter a filter number: '))
print([aa for aa in a if aa < number])