'''
Ask the user for a string and print out whether this string is a palindrome or not. (A palindrome is a string that reads the same forwards and backwards.)
'''

print('CHECKS IF WORK IS A PALINDROME')
word = input('Enter a word: ')

#CONVERTED TO ARRAY AND REVERSED
list_word = reversed(list(word))

#CONVERTED TO STRING AND REMOVED ALL COMMAS(,)
reversed_word = (','.join(list_word)).replace(',','')

#CHECKED IF ORIGINAL VS REVERSED ARE EQUAL
if word == reversed_word:
    print('{} is a PALINDROME'.format(word))
else:
    print('{} is NOT a PALINDROME'.format(word))