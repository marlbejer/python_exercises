'''
Create a program that asks the user for a number and then prints out a list of all the divisors of that number
'''
print('Enter a Number and we will give all its divisors!')
number = int(input('Enter a Number: '))
num_array = [div for div in range(1, number) if (number%div) == 0]
for num in num_array:
    print(num)
print(number)
