'''
Ask the user for a number and determine whether the number is prime or not. (For those who have forgotten, a prime number is a number that has no divisors.). You can (and should!) use your answer to Exercise 4 to help you. 
'''

def get_input():
    return int(input('Give me a number: '))

def check_if_prime(number):
    if number < 1:
        return 'Not a valid number.'
    elif number > 1: 
        for i in range(2, number):
            if number%i == 0:
                return 'Not a PRIME Number!'
    return 'PRIME Number!'

while(True):    
    print(check_if_prime(get_input()))
    if((input('Another try? Y/N ')).upper() == 'Y'):
        continue
    else:
        break