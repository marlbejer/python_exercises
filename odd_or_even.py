'''
Ask the user for a number. Depending on whether the number is even or odd, print out an appropriate message to the user
'''

print('This will determin if number is ODD or EVEN')
answer = True
while(answer):
    number = int(input('Enter A Number: '))
    if (number % 2) == 0:
        print('{} is EVEN'.format(number))
    else:
        print('{} is ODD'.format(number))
        
    again = input('Another try? Y/N')
    if(again.upper() == 'N'):
        answer = False