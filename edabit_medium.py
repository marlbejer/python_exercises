'''CALCULATE PROFIT'''
# def profit(data):
    # cost = data['cost_price'] * data['inventory']
    # sell = data['sell_price'] * data['inventory']
    # return int(round(sell - cost, 0))

# print(profit({
#   "cost_price": 32.67,
#   "sell_price": 45.00,
#   "inventory": 1200
# }))

# print(profit({
#   "cost_price": 225.89,
#   "sell_price": 550.00,
#   "inventory": 100
# }))

# print(profit({
#   "cost_price": 2.77,
#   "sell_price": 7.95,
#   "inventory": 8500
# }))

'''
International Greetings
Suppose you have a guest list of students and the country they are from, stored as key-value pairs in a dictionary.
'''
# GUEST_LIST = {
# "Randy": "Germany",
# "Karla": "France",
# "Wendy": "Japan",
# "Norman": "England",
# "Sam": "Argentina"
# }
# def greetings(name):
    # name = name.capitalize()
    # if name in GUEST_LIST:
    #     country = GUEST_LIST[name]
    #     return "Hi! I'm {}, and I'm from {}.".format(name, country)
    # else:
    #     return "Hi! I'm a guest."    
# print(greetings('randy'))
# print(greetings("Sam"))
# print(greetings("Monti"))

'''
Check If Lines Are Parallel
Given two lines, determine whether or not they are parallel.
Lines are represented by a list [a, b, c], which corresponds to the line ax+by=c.

    y = mx + b -> base from linear equation
where:
    m = slope
'''

# def lines_are_parallel(l1, l2):
    # m1 = l1[0]/l1[1]
    # m2 = l2[0]/l2[1]
    # if m1 == m2:
    #     return True
    # else:
    #     return False


'''
Quadratic Equation
Create a function to find only the positive discriminant value of x in any quadratic equation ax^2 + bx + c. 
The function will take three arguements:

a as the coefficient of x^2
b as the coefficient of x
c as the constant term
'''

# import math

# def quadratic_equation(a, b, c):
#     discriminant = (b**2) - (4*a*c)
#     x = (-b + (math.sqrt(abs(discriminant)))) / (2*a)
#     if x <= 0:
#         x = (-b - (math.sqrt(abs(discriminant)))) / (2*a)    
#     return x

# print(quadratic_equation(1, 2, -3))
# print(quadratic_equation(2, -7, 3))
# print(quadratic_equation(1, -12, -28))

'''Note: num ** 0.5 -> is like a square root'''

'''
Concatenate Variable Number of Input Lists
Create a function that concatenates n input lists, where n is variable.

concat([1, 2, 3], [4, 5], [6, 7]) ➞ [1, 2, 3, 4, 5, 6, 7]
concat([1], [2], [3], [4], [5], [6], [7]) ➞ [1, 2, 3, 4, 5, 6, 7]
concat([1, 2], [3, 4]) ➞ [1, 2, 3, 4]
concat([4, 4, 4, 4, 4]) ➞ [4, 4, 4, 4, 4]
'''

# def concat(*args):
#     new_array = []
#     for a in args:
#         new_array += a
#     return new_array
        
# print(concat([1, 2, 3], [4, 5], [6, 7]))
# print(concat([1], [2], [3], [4], [5], [6], [7]))
# print(concat([1, 2], [3, 4]))
# print(concat([4, 4, 4, 4, 4]))

'''
Is Johnny Making Progress?
To train for an upcoming marathon, Johnny goes on one long-distance run each Saturday. 
He wants to track how often the number of miles he runs exceeds the previous Saturday. 
This is called a progress day.
Create a function that takes in a list of miles run every Saturday and returns Johnny's total number of progress days.

progress_days([3, 4, 1, 2]) ➞ 2
# There are two progress days, (3->4) and (1->2)
progress_days([10, 11, 12, 9, 10]) ➞ 3
progress_days([6, 5, 4, 3, 2, 9]) ➞ 1
progress_days([9, 9])  ➞ 0
'''

# def progress_days(runs):
#     count = 0
#     for run in range(len(runs) - 1):
#         if runs[run + 1] > runs[run]:
#             count+=1
#     return count

# print(progress_days([9, 9]))
# print(progress_days([6, 5, 4, 3, 2, 9]))
# print(progress_days([10, 11, 12, 9, 10]) )

'''
All Occurrences of an Element in a List
Create a function that returns the indices of all occurrences of an item in the list.

get_indices(["a", "a", "b", "a", "b", "a"], "a") ➞ [0, 1, 3, 5]
get_indices([1, 5, 5, 2, 7], 7) ➞ [4]
get_indices([1, 5, 5, 2, 7], 5) ➞ [1, 2]
get_indices([1, 5, 5, 2, 7], 8) ➞ []
'''
# def get_indices(lst, el):
#     return [i for i in range(len(lst)) if el == lst[i]]

# print(get_indices([1, 5, 5, 2, 7], 5))
# print(get_indices(["a", "a", "b", "a", "b", "a"], "a"))
# print(get_indices([1, 5, 5, 2, 7], 7) )