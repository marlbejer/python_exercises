PYTHON EXERCISES

Date: 10/10/2019

link: https://www.practicepython.org

Due to difficulties using Python while creating a project using Tordana & PyMongo. 
I decided to focus more first on what Python can do. 
Digging deeper on its structure and functionalities by creating simple exercises.


Date: 10/11/2019

link: https://codingbat.com/python/
link: https://edabit.com/challenges/python3

1. Done the online python coding practice.

Disclaimer:
    I have tried my best to provide the expected result, 
    I will only check my work againts the SOLUTION once all exercises are complete.