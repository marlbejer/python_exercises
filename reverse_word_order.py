'''
Write a program (using functions!) that asks the user for a long string containing multiple words. Print back to the user the same string, except with the words in backwards order.
'''

def get_input():
    return input('Enter a sentence: ')

def reverse_input(sentence):
    
    #SPLITTED THE WORD -> INTO ARRAY
    sentence_array = sentence.split(' ')
    
    #REVERSED THE ARRAY USING array[::-1]
    #JOIN ELEMENTS OF ARRAY BUT SEPARATED BY COMMAS
    #REMOVED COMMAS AND REPLACED IT WITH SPACES
    reversed_sentence_array = (','.join(sentence_array[::-1])).replace(',', ' ')
    
    #RETURN THE RESULT
    return reversed_sentence_array

while(True):
    print(reverse_input(get_input()))
    if((input('Another try? Y/N ')).upper() == 'Y'):
        continue
    else:
        break