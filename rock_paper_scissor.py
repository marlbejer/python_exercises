'''
Make a two-player Rock-Paper-Scissors game. (Hint: Ask for player plays (using input), compare them, print out a message of congratulations to the winner, and ask if the players want to start a new game)
'''

import random

print('R - Rock / P - Paper / S - Scissor')

choices = ['R', 'P', 'S']
answer = True

while(answer):
    
    choice = (input('Enter choice: ')).upper()
    
    try:
        bot  = (random.choice(choices)).upper()
        
        if choice == bot:
            print('SAME {}'.format(bot))
            
        elif choice == 'R':
            if bot == 'P':
                print('You Lose!')
            else:
                print('You Win!')
            print('You - {} , Bot - {}'.format(choice, bot))    
            
        
        elif choice == 'P':
            if bot == 'R':
                print('You Win!')
            else:
                print('You Lose!')
            print('You - {} , Bot - {}'.format(choice, bot))
        
        elif choice == 'S':
            if bot == 'P':
                print('You Win!')
            else:
                print('You Lose!')
            print('You - {} , Bot - {}'.format(choice, bot))
        
        else:
            break        
    
    except:
        answer = False
        break
        
    