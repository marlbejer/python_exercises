'''
Generate a random number between 1 and 9 (including 1 and 9). Ask the user to guess the number, then tell them whether they guessed too low, too high, or exactly right. (Hint: remember to use the user input lessons from the very first exercise)
'''

import random

print('GUESS THE NUMBER, 1 - 10')
while(True):    
    random_number = random.randint(1, 10)    
    guess_number = int(input('Enter Your Guess: '))        
    while(True):        
        if guess_number == random_number:
            print('{} is the correct answer.'.format(random_number))
            break;        
        elif guess_number > random_number:
            print('LOWER')   
        else:
            print('HIGHER')            
        guess_number = int(input('Enter Your Guess: '))        
    response = input('Do you want to continue? Y/N ')    
    if response.upper() == 'Y':
        continue
    else:
        break