'''
Create a program that asks the user to enter their name and their age. Print out a message addressed to them that tells them the year that they will turn 100 years old.
'''


from datetime import datetime


class CharacterInput():

	def __init__(self, name, age):
		self.name = name
		self.age = age
  
	def compose(self):
		now = datetime.now()
		year = int(now.year)
		year100 = (100 - int(self.age)) + year
		print("{}, you'll be 100 on {}.".format((self.name).capitalize(), year100))

		
name = input('Enter Your Name: ')
age = input('Enter Your Age: ')

ci = CharacterInput(name, age)
ci.compose()