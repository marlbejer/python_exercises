'''
Take two lists, say for example these two:

  a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
  b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
and write a program that returns a list that contains only the elements that are common between the lists (without duplicates). Make sure your program works on two lists of different sizes.

Extras:

1. Randomly generate two lists to test this
2. Write this in one line of Python (don’t worry if you can’t figure this out at this point - we’ll get to it soon)
'''
import random

# # SOLUTION 1
# a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
# b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

# #CONCAT 2 ARRAYS
# new_array = a + b

# #FILTER
# filtered_array = list(dict.fromkeys(new_array))

# #SORT & PRINT
# print(sorted(filtered_array))


# SOLUTION 2

a = []
b = []

# LOAD RANDON NUMBERS
for i in range(1, 10):
    a.append(random.randint(1,20))
    b.append(random.randint(1,20))

print('List A: {}'.format(a))
print('List B: {}'.format(b))

new_array = a + b
filtered_array = list(dict.fromkeys(new_array))

print('List C: {}'.format(sorted(filtered_array)))