class Employee:
   'Common base class for all employees'
   empCount = 0

   def __init__(self, name, salary):
      self.name = name
      self.salary = salary
      Employee.empCount += 1
      # self.empCount += 1
   
   def displayCount(self):
     print("Total Employee %d" % Employee.empCount)
     # print("Total Employee %d" % self.empCount)

   def displayEmployee(self):
      print("Name : ", self.name,  ", Salary: ", self.salary)

"This would create first object of Employee class"
emp1 = Employee("Zara", 2000)
"This would create second object of Employee class"
emp2 = Employee("Manni", 5000)
emp1.displayEmployee()
emp2.displayEmployee()
print("Total Employee %d" % Employee.empCount)
# print(emp1.displayCount())

## NOTE by Marlon
''' 
      Using 'self' in variables only apply to the object what instantiate the class. If a variable is without self or declare outside of __init__ all objects that uses the same class will inherit its value.
'''

# hasattr(emp1, 'age')    # Returns true if 'age' attribute exists
# getattr(emp1, 'age')    # Returns value of 'age' attribute
# setattr(emp1, 'age', 8) # Set attribute 'age' at 8
# delattr(empl, 'age')    # Delete attribute 'age'

# print(emp1)
# print(emp1.age)

''' NOTE: 
   using the options above will give option in modify the 'self'
'''